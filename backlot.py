import sys

from PyQt5 import QtCore, QtWidgets, QtGui

NORTH = (0, 1)
EAST = (1, 0)
SOUTH = (0, -1)
WEST = (-1, 0)


class Edge(QtWidgets.QLabel):
    def __init__(self, parent, **kwargs):
        super().__init__(parent, **kwargs)

        self.setStyleSheet('background-color: white;')
        self.is_wall = False

    def SetWall(self):
        self.is_wall = True
        self.setStyleSheet('background-color: black;')

    def IsOpen(self):
        return not self.is_wall


class Cell(QtWidgets.QLabel):
    def __init__(self, grid, i, j, **kwargs):
        super().__init__(grid, **kwargs)
        self.i = i
        self.j = j

        self.grid = grid
        self.neighbors = {}
        self.edges = {}

        self.is_required = False
        self.is_recurse = False

        self.enabled = True

        font = self.font()
        font.setPointSize(32)
        self.setFont(font)
        self.setAlignment(QtCore.Qt.AlignCenter)

        self.setFixedSize(75, 75)

        self.setAttribute(QtCore.Qt.WA_Hover)

    def __repr__(self):
        return "Cell [{},{}]".format(self.i, self.j)

    def SetNeighbor(self, other, direction):
        edge = Edge(self.parentWidget())

        if direction == NORTH or direction == SOUTH:
            edge.setFixedHeight(20)
        elif direction == EAST or direction == WEST:
            edge.setFixedWidth(20)

        self.neighbors[direction] = other
        self.edges[direction] = edge

        reverse_dir = {NORTH: SOUTH, EAST: WEST,
                       SOUTH: NORTH, WEST: EAST}[direction]

        other.neighbors[reverse_dir] = self
        other.edges[reverse_dir] = edge

    def SetRequired(self):
        self.is_required = True
        self.setStyleSheet('background-color: green;')

    def SetRecurse(self):
        self.is_recurse = True
        self.setStyleSheet('background-color: blue;')

    def setEnabled(self, enabled):
        self.enabled = enabled

    def enterEvent(self, event):
        if self.enabled:
            self.grid.CellClicked(self)

    def SetSubsequent(self, cell, seq=''):
        if not cell:
            self.setText("")
            return

        icons = {(0, 0): "•", (0, 1): "⭡", (-1, 0): "⭢",
                 (0, -1): "⭣", (1, 0): "⭠"}
        self.setText(str(seq) + icons[(self.j - cell.j, self.i - cell.i)])


class Quota(QtWidgets.QLineEdit):
    def __init__(self, parent, quota, **kwargs):
        super().__init__(parent, **kwargs)

        self.setMaximumWidth(40)

        self.setReadOnly(True)
        self.quota = quota
        self.QuotaMet(0)

    def QuotaMet(self, count):
        remain = self.quota - count
        self.setText(str(remain))

        if remain == 0:
            self.setStyleSheet('background-color: red;')
            return True
        else:
            self.setStyleSheet('')
            return False


class Grid(QtWidgets.QWidget):

    kGridDim = 9

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.kDim = 9

        # Create cells
        self.rows = [[Cell(self, i, j) for j in range(Grid.kGridDim)]
                     for i in range(Grid.kGridDim)]
        self.cols = list(map(list, zip(*self.rows)))

        # Assign neighbors
        for i in range(Grid.kGridDim):
            for j in range(Grid.kGridDim - 1):
                self.rows[i][j].SetNeighbor(self.rows[i][j+1], EAST)
        for i in range(Grid.kGridDim - 1):
            for j in range(Grid.kGridDim):
                self.rows[i][j].SetNeighbor(self.rows[i+1][j], SOUTH)

        # Note row/col quotas from puzzle
        self.row_quotas = {
            0: Quota(self, 5),
            1: Quota(self, 7),
            2: Quota(self, 8),
            6: Quota(self, 6),
            7: Quota(self, 6),
            8: Quota(self, 5),
        }
        self.col_quotas = {
            0: Quota(self, 6),
            1: Quota(self, 6),
            2: Quota(self, 6),
            6: Quota(self, 7),
            7: Quota(self, 7),
            8: Quota(self, 6),
        }

        # Note walls from puzzle
        self.rows[0][1].edges[SOUTH].SetWall()
        self.rows[0][3].edges[EAST].SetWall()
        self.rows[0][7].edges[SOUTH].SetWall()
        self.rows[1][1].edges[SOUTH].SetWall()
        self.rows[1][2].edges[SOUTH].SetWall()
        self.rows[1][3].edges[SOUTH].SetWall()
        self.rows[1][4].edges[SOUTH].SetWall()
        self.rows[1][6].edges[EAST].SetWall()
        self.rows[2][4].edges[EAST].SetWall()
        self.rows[2][7].edges[SOUTH].SetWall()
        self.rows[3][0].edges[EAST].SetWall()
        self.rows[3][1].edges[EAST].SetWall()
        self.rows[3][3].edges[EAST].SetWall()
        self.rows[3][4].edges[EAST].SetWall()
        self.rows[3][5].edges[EAST].SetWall()
        self.rows[3][8].edges[SOUTH].SetWall()
        self.rows[4][0].edges[EAST].SetWall()
        self.rows[4][2].edges[EAST].SetWall()
        self.rows[4][3].edges[SOUTH].SetWall()
        self.rows[4][5].edges[EAST].SetWall()
        self.rows[4][7].edges[SOUTH].SetWall()
        self.rows[5][1].edges[EAST].SetWall()
        self.rows[5][5].edges[SOUTH].SetWall()
        self.rows[5][6].edges[EAST].SetWall()
        self.rows[6][0].edges[EAST].SetWall()
        self.rows[6][4].edges[SOUTH].SetWall()
        self.rows[6][6].edges[SOUTH].SetWall()
        self.rows[7][1].edges[SOUTH].SetWall()
        self.rows[7][1].edges[EAST].SetWall()
        self.rows[7][4].edges[SOUTH].SetWall()
        self.rows[8][2].edges[EAST].SetWall()

        # Note special cells from puzzle
        for (i, j) in [(1, 5), (2, 7), (4, 2), (4, 5), (5, 3), (6, 3), (7, 0), (7, 6), (7, 7), (8, 6), (8, 7)]:
            self.rows[i][j].SetRequired()
        for (i, j) in [(5, 4), (6, 5)]:
            self.rows[i][j].SetRecurse()

        # Set up view
        layout = QtWidgets.QGridLayout(self)
        layout.setSpacing(0)

        # Display cels and walls
        for (i, row) in enumerate(self.rows):
            for (j, cell) in enumerate(row):
                layout.addWidget(cell, 2*i, 2*j)
                if EAST in cell.edges:
                    layout.addWidget(cell.edges[EAST], 2*i, 2*j + 1)
                if SOUTH in cell.edges:
                    layout.addWidget(cell.edges[SOUTH], 2*i + 1, 2*j)

        # Display row/col quotas
        for (i, quota) in self.row_quotas.items():
            layout.addWidget(quota, 2*i, 2*self.kDim)
        for (i, quota) in self.col_quotas.items():
            layout.addWidget(quota, 2*self.kDim, 2*i)

        self.Reset()

    def UpdateQuotas(self):
        for i in range(self.kDim):
            row_count = sum([True if c.i == i else False for c in self.trail])
            if i in self.row_quotas and self.row_quotas[i].QuotaMet(row_count):
                [c.setEnabled(False) for c in self.rows[i]]

        for j in range(self.kDim):
            col_count = sum([True if c.j == j else False for c in self.trail])
            if j in self.col_quotas and self.col_quotas[j].QuotaMet(col_count):
                [c.setEnabled(False) for c in self.cols[j]]

    def Reset(self):
        # Disable all cells
        for row in self.rows:
            for c in row:
                c.SetSubsequent(None)
                c.setEnabled(False)

        # Enable N E S W cells
        self.rows[0][4].setEnabled(True)
        self.rows[4][8].setEnabled(True)
        self.rows[8][4].setEnabled(True)
        self.rows[4][0].setEnabled(True)

        self.trail = []  # A list of cells in the current trail
        self.UpdateQuotas()

    def CellClicked(self, cell):
        # Disable all cells
        for row in self.rows:
            for c in row:
                c.setEnabled(False)

        # Handle backtrack
        while cell in self.trail:
            self.trail.pop().SetSubsequent(None)

        # Update icon
        cell.SetSubsequent(cell, len(self.trail) + 1)
        if len(self.trail):
            self.trail[-1].SetSubsequent(cell, len(self.trail))

        # Add cell to trail
        self.trail.append(cell)

        # Enable neighboring cells not already on path
        for (direction, edge) in cell.edges.items():
            if edge.IsOpen() and cell.neighbors[direction] not in self.trail[0:-2]:
                cell.neighbors[direction].setEnabled(True)

        self.UpdateQuotas()

        # Backtracking always allowed
        if len(self.trail) > 1:
            self.trail[-2].setEnabled(True)


class MainWindow(QtWidgets.QWidget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.setWindowTitle("Backlot")

        grid = Grid(self)
        reset = QtWidgets.QPushButton(self, text="Reset")

        reset.clicked.connect(lambda x: grid.Reset())

        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(grid)
        layout.addWidget(reset)

        self.show()


def main():
    app = QtWidgets.QApplication(sys.argv)

    win = MainWindow()

    app.exec()


if __name__ == "__main__":
    main()
